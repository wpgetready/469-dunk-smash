﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class MainMenuUI
{
    public Image restoreBtn, shopBtn, soundBtn, leaderboardBtn, removeAdsBtn;
}

[System.Serializable]
public class GameMenuUI
{
    public Image pauseBtn, timerBg, timerFillImg, flashImage;
    public Text scoreText, coinText;
}

[System.Serializable]
public class ShopMenuUI
{
    public Image coinImage, backBtn, buyBtnCoinImg;
}

[System.Serializable]
public class GameOverMenuUI
{
    public Image shareBtn, backBtn;
    public Text scoreText, hiScoreText, coinText;
    public GameObject rewardAdsBtn;
}


public class GameUI : MonoBehaviour {

    public static GameUI instance;

    public MainMenuUI mainMenuUI;
    public GameMenuUI gameMenuUI;
    public GameOverMenuUI gameOverUI;
    public ShopMenuUI shopMenuUI;

    public Text[] mainFont, secondFont;

    public GameObject BasketHolder, player, mainMenu, gameMenu, gameOverMenu, pausePanel;
    public SpriteRenderer background;
    public Image menuBasketImg;

    private AudioSource audioS;
    private AudioClip buttonClick;

    [HideInInspector]
    public managerVars vars;

    void OnEnable()
    {
        vars = Resources.Load<managerVars>("managerVarsContainer");
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
        audioS = GetComponent<AudioSource>();
    }

    void Start()
    {
        GameManager.instance.gameOver = false;
        GameManager.instance.gameStarted = false;
        GameManager.instance.gamePaused = false;
        GameManager.instance.currentScore = 0;
        GameManager.instance.currentPoints = 0;

        if (!GameManager.instance.canShowAds) mainMenuUI.removeAdsBtn.gameObject.SetActive(false);

        gameMenuUI.coinText.text = "" + GameManager.instance.currentPoints;

        menuBasketImg.sprite = vars.characters[GameManager.instance.selectedSkin].characterShopSprite;

        Time.timeScale = 1;
        buttonClick = vars.buttonSound;
        DecideBackground();

        if (GameManager.instance.isMusicOn == true)         //if musicon is true
        {
            AudioListener.volume = 1;                       //we set valume to 1
            mainMenuUI.soundBtn.sprite = vars.soundOffButton;   //and change soundbtn img to off
        }
        else
        {
            AudioListener.volume = 0;
            mainMenuUI.soundBtn.sprite = vars.soundOnButton;
        }

        if (GameManager.instance.gameRestart)
        {
            GameManager.instance.gameRestart = false;
            PlayBtn();
        }

    }

    public void ButtonPress()
    {
        audioS.PlayOneShot(buttonClick);
    }

    void DecideBackground()
    {
        background.sprite = vars.bgThemes[Random.Range(0, vars.bgThemes.Count)].backgroundSprite;
    }

    #region MainMenu

    public void PlayBtn()
    {
        if (GameManager.instance.firstTime)
        {
            GameManager.instance.firstTime = false;

        } else
        {
            AdsManager.instance.ShowInterstitial();
        }
        ButtonPress();
        mainMenu.SetActive(false);
        gameMenu.SetActive(true);
        player.SetActive(true);
        BasketHolder.SetActive(true);
    }

    public void RestoreBtn()
    {
        ButtonPress();
#if UnityIAP
        Purchaser.instance.RestorePurchases();
#endif
    }

    public void RemoveAdsBtn()
    {
        ButtonPress();

        if (!GameManager.instance.canShowAds)
            mainMenuUI.removeAdsBtn.gameObject.SetActive(false);
        else
        {
#if UnityIAP
            Purchaser.instance.BuyNoAds();
#endif
        }
    }

    public void LeaderboardBtn()
    {
        ButtonPress();
#if UNITY_EDITOR
        Debug.Log("Test on device");
#elif UNITY_ANDROID
            GooglePlayManager.singleton.OpenLeaderboardsScore();
#elif UNITY_IOS
            LeaderboardiOSManager.instance.ShowLeaderboard();
#endif
    }

    public void SoundBtn()
    {
        ButtonPress();
        if (GameManager.instance.isMusicOn == true)
        {
            GameManager.instance.isMusicOn = false;
            AudioListener.volume = 0;
            mainMenuUI.soundBtn.sprite = vars.soundOnButton;
        }
        else
        {
            GameManager.instance.isMusicOn = true;
            AudioListener.volume = 1;
            mainMenuUI.soundBtn.sprite = vars.soundOffButton;
        }

        GameManager.instance.Save();
    }

    #endregion

    #region GameMenu

    public void PauseBtn()
    {
        ButtonPress();
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        GameManager.instance.gamePaused = true;
    }

    public void ResumeBtn()
    {
        ButtonPress();
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        GameManager.instance.gamePaused = false;
    }

    public void RateBtn()
    {
        ButtonPress();
        Application.OpenURL(vars.rateButtonUrl);
    }

    #endregion

    #region GameOverMenu

    public void ShareBtn()
    {
        ButtonPress();
        ShareScreenShot.instance.ShareMethod();
    }

    public void BackBtn()
    {
        ButtonPress();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void RetryBtn()
    {
        ButtonPress();
        GameManager.instance.gameRestart = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void RewardAdsBtn()
    {
        ButtonPress();
        UnityAds.instance.ShowRewardedAd();
        gameOverUI.rewardAdsBtn.SetActive(false);
    }

    #endregion

    public void GameOver()
    {
        StartCoroutine(GameOverCoroutine());
    }

    IEnumerator GameOverCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        if (UnityAds.instance.RewardAdReady) gameOverUI.rewardAdsBtn.SetActive(true);
        else gameOverUI.rewardAdsBtn.SetActive(false);

        BasketHolder.SetActive(false);
        player.SetActive(false);
        gameMenu.SetActive(false);
        gameOverUI.scoreText.text = "Score " + GameManager.instance.currentScore;

        if (GameManager.instance.currentScore > GameManager.instance.bestScore)
            GameManager.instance.bestScore = GameManager.instance.currentScore;

        gameOverUI.hiScoreText.text = "Best " + GameManager.instance.bestScore;

        gameOverUI.coinText.text = "" + GameManager.instance.currentPoints;

        gameOverMenu.SetActive(true);
        GameManager.instance.Save();

    }

    public IEnumerator Flash()
    {
        float alpha = 0.8f;
        gameMenuUI.flashImage.color = new Color(1, 1, 1, alpha);
        while (alpha > 0)
        {
            alpha -= Time.deltaTime * 1.5f;
            gameMenuUI.flashImage.color = new Color(1, 1, 1, alpha);
            yield return null;
        }
    }

}
