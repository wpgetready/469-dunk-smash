﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour {

    public static CoinSpawner instance;

    public float minY, maxY, minX, maxX;
    public GameObject coinPrefab;
    public float spawnTime;

    private float timeTracker;
    private bool spawnCoin = true;

    public bool SpawnCoin { set { spawnCoin = value; } }

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Use this for initialization
    void Start ()
    {
        timeTracker = spawnTime;
	}

    private void Update()
    {

        if (GameManager.instance.gameOver || !GameManager.instance.gameStarted) return;

        timeTracker -= Time.deltaTime;
        if (timeTracker <= 0 && spawnCoin)
        {
            spawnCoin = false;
            timeTracker = spawnTime;
            Spawn();
        }
    }

    private void Spawn()
    {
        float y = Random.Range(minY, maxY);
        float x = Random.Range(minX, maxX);

        Instantiate(coinPrefab, new Vector2(x, y), Quaternion.identity);
    }

}
