﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour {

    [SerializeField]
    string[] motivations;

    [SerializeField]
    Text textFieldLeft, textFieldRight, textPerfect, textPoint;

    [SerializeField]
    string nameTrigger;

    Animator animatorLeft, animatorRight, animatorPoint;

    void Awake()
    {
        animatorLeft = textFieldLeft.GetComponent<Animator>();
        animatorRight = textFieldRight.GetComponent<Animator>();
        animatorPoint = textPoint.GetComponent<Animator>();
    }

    public void Motivar(int points, int hitConsecutive, bool rightBasket)
    {
        string texto = motivations[Random.Range(0, motivations.Length - 1)] + " !\n" + "+" + points.ToString();

        if (rightBasket)
        {
            textFieldLeft.text = texto;
            animatorLeft.SetTrigger(nameTrigger);
        }
        else
        {
            textFieldRight.text = texto;
            animatorRight.SetTrigger(nameTrigger);
        }

        textPerfect.enabled = true;
        textPerfect.text = "PERFECTO X" + hitConsecutive.ToString();

        textPoint.text = "+" + points.ToString();
        animatorPoint.SetTrigger(nameTrigger);
    }
    public void Desmotivar()
    {
        textPerfect.text = "";
        textPerfect.enabled = false;
    }
}
