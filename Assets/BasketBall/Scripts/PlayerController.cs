﻿using UnityEngine;
using System.Collections;

enum Mode { normal, smoke, fire }

public class PlayerController : MonoBehaviour {

    PlayerController instance;

    private Rigidbody2D myBody;         //ref to rigidbody
    private SpriteRenderer mySprite;    //ref to border sprite
    private AudioSource audioS;         //ref to audio source

    private bool doJump = false;        //this decide if player can jump or not
    private bool moveRight = true;      //decide the x direction
    private bool hitStrong = false;      //check if player hit strong
    private int hitConsecutive = 0;
    [SerializeField]
    private Mode mode = Mode.normal;    //player mode

    CameraController myCam;

    [SerializeField]
    private float jumpForce = 5f, xForce = 1f, forceFall = -1; //forces applied to player on tap

    [SerializeField]
    private ParticleSystem smokeEffect, fireEffect; //ref to particle effects

    [SerializeField]
    private Color normal1, normal2, fire1, fire2;   //ref to color

    [SerializeField]    
    private SpriteRenderer childSprite;             //ref to inner sprite

    public bool MoveRight{ set { moveRight = value; } } //setter

    private bool trigger1, trigger2; //stores the trigger data of required triggers

    [HideInInspector]
    public managerVars vars;

    void OnEnable()
    {
        vars = Resources.Load<managerVars>("managerVarsContainer");
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;

        myCam = Camera.main.GetComponent<CameraController>();
    }

    // Use this for initialization
    void Start ()
    {
        mySprite = GetComponent<SpriteRenderer>();  //get the component refrence
        myBody = GetComponent<Rigidbody2D>();       //get the component refrence

        DecidePlayerSkin();                         

        audioS = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Teleport();     
        ChangeLookDir();
        CheckScore();

        //if we tap and doJump is false and game is not paused
        if (Input.GetMouseButtonDown(0) && doJump == false && !GameManager.instance.gamePaused && Timer.instance.time > 0)
        {
            audioS.PlayOneShot(vars.jumpSound);
            doJump = true; //doJump is true
        }
	}

    private void FixedUpdate()
    {
        if (doJump == true) Jump(); //if doJump is true we Jump
    }

    //method which ake player jump
    void Jump()
    {
        myBody.velocity = new Vector2(xForce, jumpForce);//then add the real jump force
        doJump = false;//jump is false
    }

    void Teleport()
    {
        if (transform.position.x > 3.5f)                                        //if player x pos is more than 3.5f
            transform.position = new Vector2(-3.35f, transform.position.y);     //we change its x pos to -3.35f
        else if (transform.position.x < -3.5f)                                  //if player x pos is more than -3.5f
            transform.position = new Vector2(3.35f, transform.position.y);      //we change its x pos to 3.35f
    }

    void ChangeLookDir()    //when we score we change direction
    {
        if (moveRight && mySprite.flipX == false)   //if moveRight is true and sprite flipX is false
        {
            mySprite.flipX = true;          //we set it true
            childSprite.flipX = true;       
        }
        else if (!moveRight && mySprite.flipX == true)  //if moveRight is true and sprite flipX is true
        {
            mySprite.flipX = false;         //we set it false
            childSprite.flipX = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)  //detect collision
    {
        if(collision.gameObject.CompareTag("Finish"))
        {
            if(Timer.instance.time <= 0)
            {
                //call gameover method
                GameManager.instance.gameOver = true;       //game over is true
                ShareScreenShot.instance.TakeScreenshot();  //we take screenshot
                Timer.instance.audioS.PlayOneShot(vars.deathSound);
                GameUI.instance.GameOver();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) //detect triggers
    {   
		//If I enter in the first trigger ,  and both triggers are off and vertical speed is negative (ball is falling)
        //if tag is "First" && trigger1 is false && trigger2 is false && y velocity is less than zero
        if (other.CompareTag("First") && !trigger1 && !trigger2 && myBody.velocity.y < 0f)
        {
            trigger1 = true;    //trigger1 is true
        }

		//if I touch the second trigger, and already have first one triggered and vertical speed is negative
        //if tag is "First" && trigger1 is true && trigger2 is false && y velocity is less than zero
        if (other.CompareTag("Second") && !trigger2 && trigger1 && myBody.velocity.y < 0f)
        {
            BasketHolder.instance.ActivateAnimation();
            BasketHolder.instance.DesactivarTriggers();
            trigger2 = true;    //trigger2 is true
            if (myBody.velocity.y < forceFall) hitStrong = true;
        }

        if (other.CompareTag("Coin"))
        {
            audioS.PlayOneShot(vars.coinSound);
            GameManager.instance.currentPoints++;
            GameUI.instance.gameMenuUI.coinText.text = "" + GameManager.instance.currentPoints;
            GameManager.instance.points++;
            GameManager.instance.Save();
            CoinSpawner.instance.SpawnCoin = true;
            other.gameObject.SetActive(false);
        }
    }

    void CheckScore()   //check the score
    {
        if (trigger1 && trigger2)   //if trigger1 and trigger2 is true. It means we scored
        {
            trigger1 = trigger2 = false;    //we set them false

            if (!GameManager.instance.gameStarted) GameManager.instance.gameStarted = true; //if game started is false set it to true

            audioS.PlayOneShot(vars.basketSound);
            Timer.instance.ResetTimer();    //reset time
            StartCoroutine(Selectbasket()); //select basket
            moveRight = !moveRight; //reverse bool
            xForce = -xForce;   //revers x force

            if (hitStrong)  //if collided is false
            {
                hitConsecutive++;
                StartCoroutine(GameUI.instance.Flash());
                SelectMode();   //we select mode
                PlayerMode();   //set player mode
            }

            if (!hitStrong)   //if collided is true
            {
                hitConsecutive = 0;
                mode = Mode.normal; //mode is set to normal
                smokeEffect.gameObject.SetActive(false);    //particle effect is deactivated
                fireEffect.gameObject.SetActive(false);
                mySprite.color = normal1;   //color is set to normal
                childSprite.color = normal2;
            }

            int pointsExtra = 1;

            if (mode == Mode.normal)                     //if mode is normal
            {
                myCam.AclarecerFondo();
                pointsExtra = 1;                         //score increase by 1
            }
            else if (mode == Mode.smoke)                 //if mode is smoke
            {
                myCam.AclarecerFondo();
                pointsExtra = 2;                         //score increase by 2
            }
            else if (mode == Mode.fire)                  //if mode is fire
            {
                myCam.EnrojecerFondo();
                StartCoroutine(myCam.Vibrar());
                if (hitConsecutive >= 3) pointsExtra = 8;//score increase by 8
                else pointsExtra = 4;                    //score increase by 4
            }
            GameManager.instance.currentScore += pointsExtra;
            if (hitConsecutive >= 1) BasketHolder.instance.ActivateMotivation(pointsExtra, hitConsecutive);
            else BasketHolder.instance.DesativateText();

            GameUI.instance.gameMenuUI.scoreText.text = GameManager.instance.currentScore.ToString();
            hitStrong = false;
        }
    }

    IEnumerator Selectbasket()
    {
        yield return new WaitForSeconds(0.6f); //we wait for 0.6 sec for Animation
        BasketHolder.instance.SelectBasket();   //select basket
    }

    void SelectMode()
    {
        if (mode == Mode.normal) mode = Mode.smoke;     //if mode is normal we change it to smoke
        else if (mode == Mode.smoke) mode = Mode.fire;  //if mode is smoke we change it to fire
    }

    void PlayerMode()                           
    {   
        switch (mode)
        {
            case Mode.normal:                               //if mode is normal
                smokeEffect.gameObject.SetActive(false);    //deactivate particel effects
                fireEffect.gameObject.SetActive(false);
                break;

            case Mode.smoke:                                //if mode is smoke
                smokeEffect.gameObject.SetActive(true);     //activate smoke effect
                break;

            case Mode.fire:                                 //if mode is fire
                smokeEffect.gameObject.SetActive(false);    //deactivate smoke effect
                fireEffect.gameObject.SetActive(true);      //activate fire effect
                mySprite.color = fire1;                     //change color
                childSprite.color = fire2;
                break;
        }
    }

    void DecidePlayerSkin() // this decide the player skin depending on selected skin
    {
        mySprite.sprite = vars.characters[GameManager.instance.selectedSkin].characterBorderSprite;
        childSprite.sprite = vars.characters[GameManager.instance.selectedSkin].characterInSprite;
    }

}
