﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    SpriteRenderer sr;

    [SerializeField]
    Color rojo;

    [SerializeField]
    float fuerza = 1;

    [SerializeField]
    int cantVibraciones = 10;

    int currentVibraciones;
    Vector3 posStart;

    void Start()
    {
        posStart = transform.position;
    }

	public IEnumerator Vibrar()
    {
        currentVibraciones = cantVibraciones;
        
        while (currentVibraciones > 0)
        {
            transform.position = new Vector3(Random.Range(-0.1f * fuerza, 0.1f * fuerza), Random.Range(-0.1f * fuerza, 0.1f * fuerza), transform.position.z);
            currentVibraciones--;
            yield return null;
        }

        transform.position = posStart;
    }

    public void EnrojecerFondo()
    {
        sr.color = rojo;
    }
    public void AclarecerFondo()
    {
        sr.color = Color.white;
    }
}
