﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowBall : MonoBehaviour {

    [SerializeField]
    private Transform ball;

    [SerializeField]
    private float scaleMax, scaleMin, dstMax, posY;
	
	void Update () {

        transform.position = new Vector3(ball.position.x, posY, ball.position.z);
        transform.rotation = Quaternion.identity;

        float dst = Vector2.Distance(transform.position, ball.position);
        float percentDst = dst / dstMax;
        float scale = Mathf.Lerp(scaleMax, scaleMin, percentDst);

        transform.localScale = new Vector2(scale, scale);
	}
}
