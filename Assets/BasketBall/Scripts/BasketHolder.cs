﻿using UnityEngine;

/// <summary>
/// This script controls the basket position and decide which to activate
/// </summary>

public class BasketHolder : MonoBehaviour {

    public static BasketHolder instance;

    [SerializeField]
    private GameObject basketLeft, basketRight; //ref to left and right basket
    [SerializeField]
    private Animator basketLeftAnim, basketRightAnim;
    [SerializeField]
    private GameObject triggerLeft, triggerRight;
    [SerializeField]
    private Animator ondaLeft, ondaRight;
    
    private TextManager textManager;

    private const string triggerScale = "escalar", triggerOnda = "expandir";

    [SerializeField]
    private float maxHeight, minHeight; //height range

    private bool rightBasket = true;    //check which basket it active

    private void Awake()
    {
        if (instance == null) instance = this;

        textManager = GetComponent<TextManager>();
    }

    // Use this for initialization
    void Start ()
    {
        SelectBasket();
    }

    public void SelectBasket()  
    {
        float yPos = Random.Range(minHeight, maxHeight);    //decide the y pos

        ActivateTriggers();

        if (rightBasket)    //if true
        {
            basketLeft.SetActive(false);    //left basket is deactivated
            basketRight.SetActive(true);    //right basket is activated
            basketRight.transform.position = new Vector2(basketRight.transform.position.x, yPos);   //and set its postions
        }
        else if (!rightBasket)
        {
            basketLeft.SetActive(true);
            basketRight.SetActive(false);
            basketLeft.transform.position = new Vector2(basketLeft.transform.position.x, yPos);
        }

        rightBasket = !rightBasket; //then we reverse the bool
    }

    public void ActivateAnimation()
    {
        if(rightBasket)
        {
            basketLeftAnim.SetTrigger(triggerScale);
        }
        else
        {
            basketRightAnim.SetTrigger(triggerScale);
        }
    }

    public void DesactivarTriggers()
    {
        if (rightBasket)
        {
            triggerLeft.SetActive(false);
        }
        else
        {
            triggerRight.SetActive(false);
        }
    }
    private void ActivateTriggers()
    {
        if (rightBasket)
        {
            triggerRight.SetActive(true);
        }
        else
        {
            triggerLeft.SetActive(true);
        }
    }

    public void ActivateMotivation(int points, int hitConsecutive)
    {
        textManager.Motivar(points, hitConsecutive, rightBasket);

        if (rightBasket)
        {
            ondaLeft.SetTrigger(triggerOnda);
        }
        else
        {
            ondaRight.SetTrigger(triggerOnda);
        }
    }
    public void DesativateText()
    {
        textManager.Desmotivar();
    }

}
