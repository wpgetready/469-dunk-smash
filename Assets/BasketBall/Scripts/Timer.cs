﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script track time
/// </summary>

public class Timer : MonoBehaviour {

    public static Timer instance;

    public Image fillImg;               //ref to fill image
    public Color blue, orange, red;     //color of image

    public float timeAmt = 12;           //make time
    public float timeMin = 4;
    public float timeReducer = 0.25f;

    [HideInInspector] public AudioSource audioS;         //ref to audio source

    [HideInInspector] public float time;                         //time which we track
    Animator anim;                      //animator reference

    [HideInInspector]
    public managerVars vars;

    void OnEnable()
    {
        vars = Resources.Load<managerVars>("managerVarsContainer");
    }

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    // Use this for initialization
    void Start ()
    {
        audioS = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();    //get refrence to animator
        time = timeAmt;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if game is not started or game over is true
        if (GameManager.instance.gameStarted == false || GameManager.instance.gameOver) return; //we return  

        ChangeColor();                                  //change the color of fill image

        if (time >= 0)                                  //time is more than zero
        {
            time -= Time.deltaTime;                     //we reduce time by deltaTime
            fillImg.fillAmount = time / timeAmt;        //we set fillAmount of fillImage
        }
	}

    private void ChangeColor()                          
    {
        if (fillImg.fillAmount > 0.75f)                 //if fillAmount is less than 0.75f
        {
            fillImg.color = blue;                       //we set color to blue
            anim.SetBool("danger", false);              //anim is false
        }
        //if fillAmount is between  0.75f and 0.5f
        else if (fillImg.fillAmount > 0.5f && fillImg.fillAmount < 0.75f) fillImg.color = orange;   //we set color to orange
        else if (fillImg.fillAmount < 0.5f) //if fillAmount is less than 0.5f
        {
            fillImg.color = red;                        //we set color to red
            anim.SetBool("danger", true);               //anim is true
        }
    }

    public void ResetTimer()
    {
        if (timeAmt - timeReducer >= timeMin) timeAmt -= timeReducer; //Reduce Time
        time = timeAmt; //Reset Time
    }
}
