﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class shopItemData{
	public string characterName;
	public Sprite characterInSprite;
    public Sprite characterBorderSprite;
    public Sprite characterShopSprite;
    public int characterPrice;
}

[System.Serializable]
public class BGthemeData{
    public Sprite backgroundSprite;
}

public class managerVars : ScriptableObject {

    [SerializeField]
	public List<shopItemData> characters = new List<shopItemData>();

    [SerializeField]
	public List<BGthemeData> bgThemes = new List<BGthemeData>();

    [SerializeField]
    public Sprite soundOnButton, soundOffButton, leaderboardButton, shopButton, rateButton, backButton,
        coinImg, noAdsImage, shareImage, pauseBtn, timerBg, timerFill, restoreBtn;

    [SerializeField]
    public Color32 gameOverScoreTextColor, gameOverBestScoreTextColor, inGameScoreTextColor, shopMenuDIamondTextColor,
        gameMenuDIamondTextColor, gameOverDIamondTextColor;

    [SerializeField]
	public Font mainFont, secondFont;

    [SerializeField]
    public AudioClip buttonSound, coinSound, jumpSound, basketSound, deathSound;

	// Standart Vars
	[SerializeField]
	public string adMobInterstitialID, adMobBannerID, rateButtonUrl, leaderBoardID;
	[SerializeField]
	public int showInterstitialAfter, bannerAdPoisiton;
    [SerializeField]
    public bool admobActive, googlePlayActive, unityIAP;
}
