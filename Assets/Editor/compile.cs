using UnityEngine;
using UnityEditor;
using System.Diagnostics;

/*
 * 20191013:Launching version 1.0.1 la version 1.0 tiene mal los codigos de admob.
 * 20191014: Faltaba poner el splash correspondiente a cada empresa...
 * Descubro tambien que para cambiar de Bundle a APK me faltaba activar y desactivar el flag, menudo detalle.
 * 20191025: Version transportada de Helix Jumpu directo a Alley Hoop
 * */

public class compile
{
    private static bool compileToBundle = true; //Si se compila a bundle la extension es .aab , sino es .apk

    private static string getExtension()
    {
        if (compile.compileToBundle)
        {
            EditorUserBuildSettings.buildAppBundle = true;
            return "aab";
        }
        EditorUserBuildSettings.buildAppBundle = false;
        return "apk";
    }

    public static void alleyoop()
    {

        Stopwatch sw = Stopwatch.StartNew();

        //definimos una variable global para la version de Android.
        //Atención: en este juego es preciso AGREGAR el simbolo a los ya exsitentes, como AdmobDef
        //Caso contrario no compilará correctamente.

        PlayerSettings.SetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Android, "AdmobDef;alleyoop");
        //en adelante esto es v�lido:
        /*
    void dosomething ()
    {
    #if alleyoop
        Debug.Log("Alley Oop");
    #elif spinnerfall
        Debug.Log("No es Alley Oop");
    #endif
    }

        Esta estrategia es suficiente para manejar el tema de Admob, pero ser� necesario cambiar la clase que administra Admob tambien
        Tambien es �til para cualquier cambio de texto en cualquier parte de la aplicacion (por ejemplo, la presentaci�n Spinner Fall
        que es diferente por juego)
 */
 
        //Define the basics:
        PlayerSettings.companyName = "The King Mobile";
        PlayerSettings.productName = "Alley Oop";
        PlayerSettings.applicationIdentifier = "com.thekingmobile.alleyoop";
        //20191015: Importante: bundleVersion es un identificador EXTERNO. Es decir, puede tener mugre y está todo bien.
        //PERO bundleVersionCode NECESITA tener un valor válido. Lo interesante del caso es que usando código solo me acepta enteros
        //Teoricamente debería aceptarme un string. Como sea, en adelante se debe ir incrementando bundleVersionCode y bundleVersion debería
        //o ir acompañando o tener su propia manera de incrementarse basado en algun standard a definir
        PlayerSettings.bundleVersion = "2.0";
        PlayerSettings.Android.bundleVersionCode = 2;
        //Esto es necesario para asegurar que se compile a 64 bits.
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
     //Aun me falta como hacer para que compile para 32 y 64 a la vez con un setting.
        //   PlayerSettings.SetArchitecture(BuildTargetGroup.Android,)

        #region documentacion
        /*
         * Hold the horse here, ac� hay varias cosas a explicar:
         * 1-Puse el icono de la aplicacion en una carpeta resources debajo de Sprites (asi que tenemos algo como Jumpy_Helix/Sprites/resources
         * De acuerdo a lo leido en https://docs.unity3d.com/ScriptReference/Resources.Load.html
         * la carpeta resources puede estar en CUALQUIER parte del proyecto y ser multiples a la vez. Solo hay que asegurarse de que los recursos
         * esten donde se piden.
         * 2-La extension del recurso DEBE de ser omitida , si es un png , jpg o lo que sea no se agrega
         * 3-De acuerdo a esto https://forum.unity.com/threads/multiple-versions-of-a-game-in-one-unity-project.207513/
         * para poner los iconos de la aplicacion sera necesario tener un array de iconos a asignar adecuadamente.
         */

        #endregion

        //Setear el icono del juego.
        /*
        Texture2D icon = Resources.Load<Texture2D>("spinner-fall-icon");
        if (icon==null)
        {
            Debug.LogError("ERROR!: el icono de spinner fall no se encuentra en resources: ver spinner-fall-icon");
            return;
        }
        Texture2D[] icons = new Texture2D[] { icon ,icon,icon,icon};
        UnityEditor.PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Android, icons);
        */

        //Cambiar el icono
        FileUtil.ReplaceFile("Assets/Basketball/Sprites/resources/alley-oop-icon.png", "Assets/Basketball/Sprites/icon.png");
        //Cambiar la pantalla de splash
        FileUtil.ReplaceFile("Assets/Basketball/Sprites/resources/the-king-mobile-splash.png", "Assets/Basketball/Sprites/logo-splash.png");
        AssetDatabase.Refresh();               

        
        
        //Indico cual es la escena base
        string[] levels = { "Assets/Basketball/Scenes/GameScene.unity" };
        //Indico el apk de salida (como sera para los google bundles?)
        //IMPORTANTE!!!!! Si se especifica App Bundle en Unity la salida tiene extension .aab sino es .apk
        string deployPath = "../other/alley-oop-release." + getExtension(); //must be an apk name complete.
        //Detallo los datos para acceder al keystore
		PlayerSettings.keyaliasPass = "kfzao0";
		PlayerSettings.keystorePass = "kfzorrilla";
		PlayerSettings.Android.keyaliasName = "469-alley-oop"; //Nota: cada juego SIEMPRE debe tener el nro asociado.
		PlayerSettings.Android.keyaliasPass = "kfzao0"; //No se si debe repetirse pero lo repito. Determinar que va y que no.
        //Atencion que es diferente por aplicacion!
		PlayerSettings.Android.keystoreName = "d:\\ESPACIO-TRABAJO\\20170502-KMFactory\\PROD\\469-dunk-smash\\other\\KEYSTORE-ALLEY-OOP.keystore";
		PlayerSettings.Android.keystorePass = "kfzorrilla"; //No se si deber repetirse, determinar cual es el posta.

        //20191025: esto es especifico de este juego. El uso de vars permite asignar dinamicamente los códigos de los avisos
        //En otros casos, es necesario usar #if y simbolos apropiados.
        managerVars vars;
        vars = Resources.Load<managerVars>("managerVarsContainer");
        //ca-app-pub-3654628576200837/3184313761 banner
        //ca-app-pub-3654628576200837/6991696832 inter
        vars.adMobBannerID = "ca-app-pub-3654628576200837/3184313761";
        vars.adMobInterstitialID = "ca-app-pub-3654628576200837/6991696832";
        vars.SetDirty();


        string[] parameters = System.Environment.GetCommandLineArgs();
        foreach (string p in parameters)
        {
            if (p.Equals("-compile"))
            {
                UnityEngine.Debug.Log("ENVIANDO A COMPILAR!!!");
                BuildPipeline.BuildPlayer(levels, deployPath, BuildTarget.Android, BuildOptions.None);
            }
        }
        
        //Bien, mas o menos tengo cierto control para compilar diferentes versiones, pero desde luego faltan cosas a saber:
        /*
         1-Es preciso poder cambiar algunos objetos de valor que es donde se encuentra el nombre de la aplicaci�n.
         2-Los codigos de Admob necesitan cambiarse.
         Existe una soluci�n ingeniosa a esto.

        Hay que tener en cuenta que DE AHORA EN ADELANTE, LA COMPILACION SIEMPRE TIENE QUE SER POR LINEA DE COMANDO

        Este ser� un batch SIMILAR a este, con los cambios adecuados:

        set prjpath =%cd%
        @echo %prjpath%
        "C:\Program Files\Unity2017.4.22\Editor\Unity.exe" -quit -batchmode -logfile stdout.log  -projectPath 
        "d:\ESPACIO-TRABAJO\20170502-KMFactory\PROD\539-helix-jumpy\codexx\" %prjpath% -executeMethod compile.SpinnerFall

        Desde luego tiene que ser un batch por cada una de las versiones y es preciso cambiar cuidadosamente lo ultimo (compile.SpinnerFall)

        OK! Asi que si tenemos un poco de cuidado,EFECTIVAMENTE SE PODRIAN COMPILAR LAS 3 VERSIONES POR SEPARADO!

         */

        sw.Stop();
        UnityEngine.Debug.Log("Tiempo total: " + sw.ElapsedMilliseconds / 1000 + "  segundos");
    }

    /*
     20191012: Verifico que el tema de los iconos no funciona como se discute en
     https://answers.unity.com/questions/235371/programmatically-set-default-icon-for-mobile.html
     Cambio la tactica y uso FileUtil.CopyFileorDirectory y listo
     
     */
    public static void hopdown()
    {
        Stopwatch sw = Stopwatch.StartNew();

        //definimos una variable global para la version de Android.
        PlayerSettings.SetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Android, "hopdown");

        //Define the basics:
        PlayerSettings.companyName = "Betty Apps";
        PlayerSettings.productName = "Hop Down";
        PlayerSettings.applicationIdentifier = "com.bettyapps.hopdown";
        PlayerSettings.bundleVersion = "1.0.1";
        PlayerSettings.Android.bundleVersionCode = 1; //Este valor debe incrementarse por cada nueva version, bundleVersion puede figurar cualquier cosa

        FileUtil.ReplaceFile("Assets/Jumpy_Helix/Sprites/resources/hop-down-icon.png", "Assets/Jumpy_Helix/Sprites/icon-app.png");
        //Cambiar la pantalla de splash
        FileUtil.ReplaceFile("Assets/Jumpy_Helix/Sprites/resources/betts-app-splash.png", "Assets/Jumpy_Helix/Sprites/logo-splash.png");

        AssetDatabase.Refresh();               

        //Indico cual es la escena base
        string[] levels = { "Assets/Jumpy_Helix/Scenes/Gameplay.unity" };
        //Indico el apk de salida (como sera para los google bundles?)
        string deployPath = "../other/hop-down-release." + getExtension() ; //must be an apk name complete.
                                                                 //Detallo los datos para acceder al keystore
        PlayerSettings.keyaliasPass = "kfzhd0";
        PlayerSettings.keystorePass = "kfzorrilla";
        PlayerSettings.Android.keyaliasName = "539-hop-down";
        PlayerSettings.Android.keyaliasPass = "kfzhd0";
        //Atencion que ser� diferente por aplicaci�n!
        PlayerSettings.Android.keystoreName = "d:\\ESPACIO-TRABAJO\\20170502-KMFactory\\PROD\\539-helix-jumpy\\other\\KEYSTORE-HOP-DOWN.keystore";
        PlayerSettings.Android.keystorePass = "kfzorrilla";
        string[] parameters = System.Environment.GetCommandLineArgs();
        foreach (string p in parameters)
        {
            if (p.Equals("-compile"))
            {
                UnityEngine.Debug.Log("ENVIANDO A COMPILAR!!!");
                BuildPipeline.BuildPlayer(levels, deployPath, BuildTarget.Android, BuildOptions.None);
            }
        }
        sw.Stop();
        UnityEngine.Debug.Log("Tiempo total: " + sw.ElapsedMilliseconds / 1000 + " segundos");
    }

    public static void pylonball()
    {
        Stopwatch sw = Stopwatch.StartNew();
        
        //definimos una variable global para la version de Android.
        PlayerSettings.SetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Android, "pylonball");

        //Define the basics:
        PlayerSettings.companyName = "Apps from Alex";
        PlayerSettings.productName = "Pylon Ball";
        PlayerSettings.applicationIdentifier = "com.appsfromalex.pylonball";
        PlayerSettings.bundleVersion = "1.0.1";
        PlayerSettings.Android.bundleVersionCode = 1; //Este valor debe incrementarse por cada nueva version, bundleVersion puede figurar cualquier cosa

        //Setear el icono del juego.

        FileUtil.ReplaceFile("Assets/Jumpy_Helix/Sprites/resources/pylon-ball-icon.png", "Assets/Jumpy_Helix/Sprites/icon-app.png");
        //Cambiar la pantalla de splash
        FileUtil.ReplaceFile("Assets/Jumpy_Helix/Sprites/resources/apps-from-alex-splash.png", "Assets/Jumpy_Helix/Sprites/logo-splash.png");

        AssetDatabase.Refresh();


        //Indico cual es la escena base
        string[] levels = { "Assets/Jumpy_Helix/Scenes/Gameplay.unity" };
        //Indico el apk de salida (como sera para los google bundles?)
        string deployPath = "../other/pylon-ball-release." + getExtension(); //must be an apk name complete.
                                                             //Detallo los datos para acceder al keystore
        PlayerSettings.keyaliasPass = "kfzpb0";
        PlayerSettings.keystorePass = "kfzorrilla";
        PlayerSettings.Android.keyaliasName = "539-pylon-ball";
        PlayerSettings.Android.keyaliasPass = "kfzpb0";
        //Atencion que ser� diferente por aplicaci�n!
        PlayerSettings.Android.keystoreName = "d:\\ESPACIO-TRABAJO\\20170502-KMFactory\\PROD\\539-helix-jumpy\\other\\KEYSTORE-PYLON-BALL.keystore";
        PlayerSettings.Android.keystorePass = "kfzorrilla";

        string[] parameters = System.Environment.GetCommandLineArgs();
        foreach (string p in parameters)
        {
            if (p.Equals("-compile"))
            {
                UnityEngine.Debug.Log("ENVIANDO A COMPILAR!!!");
                BuildPipeline.BuildPlayer(levels, deployPath, BuildTarget.Android, BuildOptions.None);
            }
        }
        sw.Stop();
        UnityEngine.Debug.Log("Tiempo total: " + sw.ElapsedMilliseconds/1000 + "  segundos");

    }
}
